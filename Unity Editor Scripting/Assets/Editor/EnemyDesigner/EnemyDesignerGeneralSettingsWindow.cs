﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EnemyDesignerGeneralSettingsWindow : EditorWindow {
	#region Variables
    public enum SettingsType {
        MAGE,
        ROGUE,
        WARRIOR
    }

    private static SettingsType dataSetting;
    private static EnemyDesignerGeneralSettingsWindow window;

    #endregion

    #region Event methods
    private void OnGUI() {
        switch ( dataSetting ) {
            case SettingsType.MAGE:
                DrawSettings( (CharacterData) EnemyDesignerWindow.MageInfo );
                break;

            case SettingsType.ROGUE:
                DrawSettings( (CharacterData) EnemyDesignerWindow.RogueInfo );
                break;

            case SettingsType.WARRIOR:
                DrawSettings( (CharacterData) EnemyDesignerWindow.WarriorInfo );
                break;
        }
    }
    #endregion

    #region Public methods
    public static void OpenWindow( SettingsType _setting ) {
        dataSetting = _setting;
        window = (EnemyDesignerGeneralSettingsWindow)GetWindow(
                        typeof(EnemyDesignerGeneralSettingsWindow) );
        window.minSize = new Vector2( 250, 200 );
        window.Show();
    } 
	#endregion

	#region Private methods
    private void DrawSettings( CharacterData _charData ) {
        GUILayout.Label("Creating a " + dataSetting + " enemy.");

        // PREFAB
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Enemy prefab: ");
        _charData.prefab = (GameObject) EditorGUILayout.ObjectField(_charData.prefab,
                                typeof(GameObject), false);

        EditorGUILayout.EndHorizontal();

        
        // MAX HEALTH
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Max health: ");
        _charData.maxHealth = EditorGUILayout.FloatField( _charData.maxHealth );

        EditorGUILayout.EndHorizontal();


        // MAX ENERGY
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Max. energy: ");
        _charData.maxEnergy = EditorGUILayout.FloatField( _charData.maxEnergy );

        EditorGUILayout.EndHorizontal();

        // POWER
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Power: ");
        _charData.power = EditorGUILayout.Slider(_charData.power, 0, 100);

        EditorGUILayout.EndHorizontal();


        // CRITIC CHANCE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Critic chance %: ");
        _charData.critChance = EditorGUILayout.Slider(_charData.critChance, 0, 100 -_charData.power);

        EditorGUILayout.EndHorizontal();


        // NAME
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Enemy name: ");
        _charData.characterName = EditorGUILayout.TextField( _charData.characterName );
        
        EditorGUILayout.EndHorizontal();



        if (_charData.prefab == null) {
            EditorGUILayout.HelpBox("This enemy needs a [Prefab] before it can be created.",
                    MessageType.Warning);


        } else if ( string.IsNullOrEmpty( _charData.characterName ) ) {
            EditorGUILayout.HelpBox("This enemy needs a [Name] before it can be created.",
                    MessageType.Warning);


        } else if (GUILayout.Button("Finish and save", GUILayout.Height(30))) {
            SaveCharacterData();
            window.Close();
        }
    }

    private void SaveCharacterData() {
        string prefabPath;
        string newPrefabPath = "Assets/prefabs/characters/";
        string dataPath = "Assets/resources/characterData/data/";

        switch (dataSetting) {
            case SettingsType.MAGE:
                // create the asset file
                dataPath += "mage/" + EnemyDesignerWindow.MageInfo.characterName + ".asset";
                AssetDatabase.CreateAsset( EnemyDesignerWindow.MageInfo, dataPath );

                // creat prefab file
                newPrefabPath += "mage/" + EnemyDesignerWindow.MageInfo.characterName + ".prefab";
                // get prefab path
                prefabPath = AssetDatabase.GetAssetPath(EnemyDesignerWindow.MageInfo.prefab);
                
                AssetDatabase.CopyAsset( prefabPath, newPrefabPath );
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                GameObject magePrefab = (GameObject)AssetDatabase.LoadAssetAtPath(
                        prefabPath, typeof(GameObject));

                if( !magePrefab.GetComponent<Mage>() )
                    magePrefab.AddComponent(typeof(Mage));
                

                magePrefab.GetComponent<Mage>().mageData = EnemyDesignerWindow.MageInfo;

                break;


            case SettingsType.ROGUE:
                // create the asset file
                dataPath += "rogue/" + EnemyDesignerWindow.RogueInfo.characterName + ".asset";
                AssetDatabase.CreateAsset(EnemyDesignerWindow.RogueInfo, dataPath);

                // creat prefab file
                newPrefabPath += "rogue/" + EnemyDesignerWindow.RogueInfo.characterName + ".prefab";
                // get prefab path
                prefabPath = AssetDatabase.GetAssetPath(EnemyDesignerWindow.RogueInfo.prefab);

                AssetDatabase.CopyAsset(prefabPath, newPrefabPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                GameObject roguePrefab = (GameObject)AssetDatabase.LoadAssetAtPath(
                        prefabPath, typeof(GameObject));

                if (!roguePrefab.GetComponent<Mage>())
                    roguePrefab.AddComponent(typeof(Mage));


                roguePrefab.GetComponent<Rogue>().rogueData = EnemyDesignerWindow.RogueInfo;

                break;

            case SettingsType.WARRIOR:
                // create the asset file
                dataPath += "warrior/" + EnemyDesignerWindow.WarriorInfo.characterName + ".asset";
                AssetDatabase.CreateAsset(EnemyDesignerWindow.WarriorInfo, dataPath);

                // creat prefab file
                newPrefabPath += "warrior/" + EnemyDesignerWindow.WarriorInfo.characterName + ".prefab";
                // get prefab path
                prefabPath = AssetDatabase.GetAssetPath(EnemyDesignerWindow.WarriorInfo.prefab);

                AssetDatabase.CopyAsset(prefabPath, newPrefabPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                GameObject warriorPrefab = (GameObject)AssetDatabase.LoadAssetAtPath(
                        prefabPath, typeof(GameObject));

                if (!warriorPrefab.GetComponent<Warrior>())
                    warriorPrefab.AddComponent(typeof(Warrior));


                warriorPrefab.GetComponent<Warrior>().warriorData = EnemyDesignerWindow.WarriorInfo;


                break;
        }
    }
    #endregion
}
