﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Types;

public class EnemyDesignerWindow : EditorWindow {
    #region Variables
    private Texture2D headerSectionTexture;
    private Texture2D mageSectionTexture;
    private Texture2D warriorSectionTexture;
    private Texture2D rogueSectionTexture;

    private Color headerSectionColor = new Color( 13f/255f, 32f/255f, 44f/255f, 1f);

    private Rect headerSection;
    private Rect mageSection;
    private Rect rogueSection;
    private Rect warriorSection;

    private static MageData mageData;
    private static RogueData rogueData;
    private static WarriorData warriorData;

    public static MageData MageInfo {   get {   return mageData;   }    }
    public static RogueData RogueInfo { get { return rogueData; } }
    public static WarriorData WarriorInfo { get { return warriorData; } }
    #endregion

    #region Event methods
    private void OnEnable() {
        InitializeTextures();
        InitializeData();
    }

    private void OnGUI() {
        DrawLayouts();
        DrawHeader();
        DrawMageSettings();
        DrawWarriorSettings();
        DrawRogueSettings();
    }
    #endregion

    #region Public methods
    #endregion

    #region Private methods
    [MenuItem("Window/Enemy Designer")]
    private static void OpenWindow() {
        EnemyDesignerWindow window =
            (EnemyDesignerWindow) GetWindow( typeof(EnemyDesignerWindow) );

        window.minSize = new Vector2( 600, 300 );
        window.Show();
    }

    private void InitializeTextures () {
        headerSectionTexture = new Texture2D(1, 1);
        headerSectionTexture.SetPixel(0, 0, headerSectionColor);
        headerSectionTexture.Apply();

        mageSectionTexture = Resources.Load<Texture2D>("Textures/MageTexture");
        rogueSectionTexture = Resources.Load<Texture2D>("Textures/RogueTexture");
        warriorSectionTexture = Resources.Load<Texture2D>("Textures/WarriorTexture");
    }

    private void InitializeData() {
        mageData = (MageData) ScriptableObject.CreateInstance(typeof(MageData));
        rogueData = (RogueData) ScriptableObject.CreateInstance(typeof(RogueData));
        warriorData = (WarriorData) ScriptableObject.CreateInstance(typeof(WarriorData));
    }



    private void DrawLayouts() {
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = Screen.width;
        headerSection.height = 50;

        mageSection.x = 0;
        mageSection.y = 50;
        mageSection.width = Screen.width / 3f;
        mageSection.height = Screen.height - 50;

        warriorSection.x = Screen.width / 3f;
        warriorSection.y = 50;
        warriorSection.width = Screen.width / 3f;
        warriorSection.height = Screen.height - 50;

        rogueSection.x = (Screen.width / 3f) * 2;
        rogueSection.y = 50;
        rogueSection.width = Screen.width / 3f;
        rogueSection.height = Screen.height - 50;

        GUI.DrawTexture( headerSection, headerSectionTexture );
        GUI.DrawTexture( mageSection, mageSectionTexture );
        GUI.DrawTexture( rogueSection, rogueSectionTexture );
        GUI.DrawTexture( warriorSection, warriorSectionTexture );
    } 

    private void DrawHeader() {
        GUILayout.BeginArea(headerSection);

        GUILayout.Label("Enemy Designer");

        GUILayout.EndArea();
    }

    private void DrawMageSettings() {
        GUILayout.BeginArea(mageSection);

        GUILayout.Label("Mage");

        // WEAPON TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Weapon type: ");
        mageData.wpnType = (MageWpnType)EditorGUILayout.EnumPopup( mageData.wpnType );

        EditorGUILayout.EndHorizontal();


        // DAMAGE TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Damage type: ");
        mageData.dmgType = (MageDmgType)EditorGUILayout.EnumPopup(mageData.dmgType);

        EditorGUILayout.EndHorizontal();


        if( GUILayout.Button( "Create!", GUILayout.Height(40)  ) ) {
            EnemyDesignerGeneralSettingsWindow.OpenWindow(
                EnemyDesignerGeneralSettingsWindow.SettingsType.MAGE );
        }


        GUILayout.EndArea();
    }

    private void DrawRogueSettings() {
        GUILayout.BeginArea(rogueSection);

        GUILayout.Label("Rogue");

        // STRATEGY TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Strategy type: ");
        rogueData.strategyType = (RogueStrategyType) EditorGUILayout.EnumPopup( rogueData.strategyType );

        EditorGUILayout.EndHorizontal();


        // WEAPON TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Weapon type: ");
        rogueData.wpnType = (RogueWpnType) EditorGUILayout.EnumPopup( rogueData.wpnType );

        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Create!", GUILayout.Height(40))) {
            EnemyDesignerGeneralSettingsWindow.OpenWindow(
                EnemyDesignerGeneralSettingsWindow.SettingsType.ROGUE);
        }

        GUILayout.EndArea();
    }

    private void DrawWarriorSettings() {
        GUILayout.BeginArea(warriorSection);

        GUILayout.Label("Warrior");

        // WEAPON TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Weapon type: ");
        warriorData.wpnType = (WarriorWpnType) EditorGUILayout.EnumPopup(warriorData.wpnType);

        EditorGUILayout.EndHorizontal();


        // CLASS TYPE
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Class type: ");
        warriorData.classType = (WarriorClassType) EditorGUILayout.EnumPopup(warriorData.classType);
        
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Create!", GUILayout.Height(40))) {
            EnemyDesignerGeneralSettingsWindow.OpenWindow(
                EnemyDesignerGeneralSettingsWindow.SettingsType.WARRIOR);
        }

        GUILayout.EndArea();
    }
    #endregion
}
