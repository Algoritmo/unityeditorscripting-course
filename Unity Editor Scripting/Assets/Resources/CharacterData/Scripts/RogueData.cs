﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

[CreateAssetMenu(fileName = "New Rogue Data", menuName = "Character Data/Rogue")]
public class RogueData : CharacterData {
    #region Variables
    public RogueStrategyType strategyType;
    public RogueWpnType wpnType;
	#endregion
    
	#region Public methods
	#endregion

	#region Private methods
	#endregion
}
