﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterData : ScriptableObject {
    #region Variables
    public GameObject prefab;
    public float maxHealth;
    public float maxEnergy;
    public float critChance;
    public float power;
    public string characterName;
	#endregion
    
	#region Public methods
	#endregion

	#region Private methods
	#endregion
}
