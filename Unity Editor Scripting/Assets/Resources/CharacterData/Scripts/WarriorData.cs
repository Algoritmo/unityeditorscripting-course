﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

[CreateAssetMenu(fileName = "New Warrior Data", menuName = "Character Data/Warrior")]
public class WarriorData : CharacterData {
    #region Variables
    public WarriorClassType classType;
    public WarriorWpnType wpnType;
	#endregion
    
	#region Public methods
	#endregion

	#region Private methods
	#endregion
}
