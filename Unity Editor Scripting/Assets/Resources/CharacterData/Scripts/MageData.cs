﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

[CreateAssetMenu(fileName = "New Mage Data", menuName = "Character Data/Mage")]
public class MageData : CharacterData {
    #region Variables
    public MageDmgType dmgType;
    public MageWpnType wpnType;
	#endregion
    
	#region Public methods
	#endregion

	#region Private methods
	#endregion
}
